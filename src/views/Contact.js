import React from 'react';
import ContactForm from '../components/contact/ContactForm';
import Navigation from '../components/navbar/Navigation';

class Contact extends React.Component {
  render() {
    return(
      <React.Fragment>
        <Navigation/>
        <ContactForm />
      </React.Fragment>
    )
  }
}

export default Contact;
