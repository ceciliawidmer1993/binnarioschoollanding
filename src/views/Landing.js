import React from 'react';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';
import styled from 'styled-components';
import { Hidden, Grid, Button } from '@material-ui/core';
import {ParagraphBlue, ParagraphLightBlue, TitleH1} from "../styling/Styles";
import sampleLandingPage from "../text/sample-landing-page.js";
import * as ROUTES from '../config/routes';
import Navigation from '../components/navbar/Navigation';


const styles = (theme) => ({
  container: {
    //padding: '4em 0',
  },
  infoContainer: {
    //padding: '2em',
    marginTop: '3em',
    justifyContent: 'center',
  },
  buttonContainer: {
    justifyContent: 'center',
  },
  explanationContainer: {
    paddingRight: '0em',
  },
  explanationParagraph: {
    lineHeight: '25px',
    textAlign: 'center',
    justifyContent: 'center',
  },
  contactButton: {
    marginTop: '5em',
    marginBottom: '5em',
    borderRadius: '1em',
    textTransform: 'none',
    width: '100%',
    fontFamily: 'Lato, sans-serif',
    
  },
  responsiveBackground: {
    background: 'FFFFFF',
      height: '100%',
 
    },
});


const ImageLogo = styled.img` && {
  width: 60%;
  object-fit: scale-down;
`;

const ImageLogoPhone = styled.img` && {
  width: 100%;
  object-fit: scale-down;
  padding: 0em;
`;


class Landing extends React.Component {

  render() {
    const { classes } = this.props;
    return (
      
      <div className={classes.responsiveBackground}>
        <Navigation/>
        <Hidden smDown>
          <Grid item container xs={12} justify="center" className={classes.container}>
            <Grid item  container xs={6}   className={classes.infoContainer}>
              <ParagraphLightBlue className={classes.explanationParagraph}> Muy pronto podrás conocer </ParagraphLightBlue>
              <ImageLogo src='/images/LogoBinarioAzul.png' />
            </Grid>

            <Grid item container xs={9}  justify="center"> 
              <Grid item   xs={9}  className={classes.explanationContainer}>
                  <ParagraphBlue className={classes.explanationParagraph}> App web a disposición de la principales universidades del mundo para facilitar el aprendizaje de los futuros Data Scientist, que podrán competir en el desarrollo de modelos de machine learning junto a sus compañeros de forma interactiva.
                  </ParagraphBlue>
              </Grid>      
            </Grid>
            
            <Grid item container xs={4}  justify="center"> 
                  <Button 
                  color="secondary" 
                  variant="contained" 
                  className={classes.contactButton} 
                  onClick={() => this.props.history.push(ROUTES.CONTACT)}>
                    Contáctanos
                  </Button>
            </Grid>
          </Grid>
        </Hidden>

        {/* what we want to SHOW when resposive view (phone)*/}
        <Hidden mdUp>
          <Grid item container xs={12} justify="center" className={classes.container}> 
            <Grid item  xs={9}  container  className={classes.infoContainer}>
              <ParagraphLightBlue className={classes.explanationParagraph}> Muy pronto podrás conocer </ParagraphLightBlue>
              <ImageLogoPhone src='/images/LogoBinarioAzul.png' />
            </Grid>

            <Grid item container xs={9}  justify="center"> 
              <Grid item   xs={9}  className={classes.explanationContainer}>
                  <ParagraphBlue className={classes.explanationParagraph}> App web a disposición de la principales universidades del mundo para facilitar el aprendizaje de los futuros Data Scientist, que podrán competir por el mejor modelo junto a sus compañeros de forma interactiva.
                  </ParagraphBlue>
              </Grid>      
            </Grid>
            
            <Grid item container xs={9}  justify="center"> 
                  <Button 
                  color="secondary" 
                  variant="contained" 
                  className={classes.contactButton} 
                  onClick={() => this.props.history.push(ROUTES.CONTACT)}>
                    Contáctanos
                  </Button>
            </Grid>
            </Grid>
        </Hidden>
     </div>
  
    )
  }
}


export default withStyles(styles)(withRouter(Landing));

