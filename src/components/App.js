import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import LandingPage from '../views/Landing';
import Contact from '../views/Contact.js';
import * as ROUTES from '../config/routes';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#2B3E7F',
      light: '#c8c8f5',
    },
    secondary: {
      main: '#42B5C3',
    },
    error: {
      main: '#e6423a',
    },
  },
})

class App extends React.Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Router>
          <div>
            <Route exact path={ROUTES.LANDING} component={LandingPage} />
            <Route exact path={ROUTES.CONTACT} component={Contact} />
          </div>
        </Router>
      </MuiThemeProvider>
    )
  }
}

//<Route exact path={ROUTES.ADMIN} component={AdminPage} />
//<Route exact path={ROUTES.PARSETEXT} component={TextParser} />

export default (App);

