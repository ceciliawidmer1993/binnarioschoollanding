import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Logo from '../../styling/Logo';
import Grid from '@material-ui/core/Grid';
import Tabs from './Tabs';

const styles = (theme) => ({
  container: {
    height: '6em',
    background: '#F0F0F0',
    //color for responsive view
    [theme.breakpoints.up('sm')]: 
    {
      backgroundColor: 'white',
      //color for normal view 
    },
  }
  ,
});

class Navigation extends React.Component {
  state = {
    drawerOpen: false,
  }

  render() {
    const { classes } = this.props;
    return (
      <Grid container alignItems="center" className={classes.container} >    
        <Grid item xs={12} sm={12} md={2}>
          <Logo white/>
        </Grid>
        <Grid item xs={12} sm={12} md={2}>
          <Tabs/>
        </Grid>
      </Grid>
    )
  }
}

export default withStyles(styles)(Navigation);
