import React from "react";
import { TitleH1 } from "../../styling/Styles";
import * as ROUTES from '../../config/routes';
import { withRouter } from 'react-router';
import {Grid} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';


const styles = (theme) => ({
  container: {
    justifyContent: 'left',
    marginRight: '20em',
  },
});

class Tabs extends React.Component {

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Grid item container xs={12}  className={classes.container}>
        <TitleH1 onClick={() => this.props.history.push(ROUTES.LANDING)}>INICIO</TitleH1>
        <TitleH1 onClick={() => this.props.history.push(ROUTES.CONTACT)}>CONTACTO</TitleH1>
        </Grid>
      </React.Fragment>
    )
  }
}



export default withStyles(styles)(withRouter(Tabs));
