import styled from 'styled-components';

// H1 size 70 pt
export const TitleH1 = styled.h1`
  font-size: 16px;
  color: #273E80;
  font-family: Lato, sans-serif;
  margin-left: 7%;
  cursor: pointer;
  &&:hover {
    background: #A6BE13;
    color: white //CDCCF8  2b3e7f
} 
`;
//azul
export const ParagraphBlue = styled.p`
  color: #273E80;
  font-size: 24px;
  font-family: Lato, sans-serif;
  line-height: 1em;
`;
//celeste
export const ParagraphLightBlue = styled.p`
  color: #A6BE13;
  font-size: 40px;
  font-family: Lato, sans-serif;
  font-weight: Bold;
  line-height: 1em;
`;
//



